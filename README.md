# Android App for Interfacing with an RTC unit via Bluetooth #

### Overview ###

* This is a sample Android Application which implements the decoupled version of the [Bluetooth Serial Service](https://bitbucket.org/lvanheerden/bluetooth-serial-service) for Android.  It interacts with an Arduino which has a Bluetooth Shield and a DS3232 Realtime Clock Unit in order to display the RTC's time on the Android
* Version 1.0

### How do I get set up? ###

Standard Android Build.  Deploy to the Android device, click on the context menu in order to select your Arduino.  If connected successfully it will display the RTC time.

### Dependencies ###
Bluetooth Enabled Arduino with a DS3232 RTC unit which has the TestRTC program loaded which can be found in the [DS3232 RTC Library](https://github.com/rweather/arduinolibs/tree/master/libraries/RTC)