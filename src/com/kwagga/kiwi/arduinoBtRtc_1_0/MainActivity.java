package com.kwagga.kiwi.arduinoBtRtc_1_0;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;

import com.kwagga.kiwi.android.bluetooth.BTSerialService;

import es.pymasde.blueterm.DeviceListActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
    // Logging
    private static final String TAG = "MainActivity";
    
    // Intent identifiers
	private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;

	private static BTSerialService btService = null;
    private final BTHandler btHandler = new BTHandler(this);
	
	private MenuItem menuItemConnect;
	
	private String deviceName;
	private boolean enablingBT;
	
	private ByteArrayOutputStream readBuffer = new ByteArrayOutputStream();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.d(TAG, "In onCreate()");
		
		setContentView(R.layout.activity_main);
		
		btService = new BTSerialService(btHandler);
	}
	
    @Override
    public void onStart() {
    	super.onStart();
    	enablingBT = false;
    }
    
    @Override
    public synchronized void onResume() {
    	
    	super.onResume();
    	
    	Log.d(TAG, "...In onResume - Attempting client connect...");
    	btService.onResume();
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	btService.stop();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		menuItemConnect = menu.getItem(0);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_connect) {
			
			Log.d(TAG, "Connect clicked");
			if (btService.getState() == BTSerialService.STATE_NONE) {
				
				Log.d(TAG, "No bt state");
				
    			// launch the DeviceListActivity to see devices and do scan    			
    			Intent deviceListIntent = new Intent(this, DeviceListActivity.class);
    			startActivityForResult(deviceListIntent, REQUEST_CONNECT_DEVICE);
			}
			else if (btService.getState() == BTSerialService.STATE_CONNECTED) {
				Log.d(TAG, "bt state connected");
				btService.stop();
				btService.start();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Sets the icon and title for the Connect menu item
	 * @param icon <Drawable> the icon reference
	 * @param title <String> the title/label of the menu item
	 */
	private void setConnectMenuView(int iconRef, int titleRef) {
		if (menuItemConnect != null) {
			menuItemConnect.setIcon(iconRef);
			menuItemConnect.setTitle(titleRef);
		}
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		Log.d(TAG, "...onActivityResult " + resultCode);
		
		switch (requestCode) {
		
		case REQUEST_ENABLE_BT :
			// Request to enable Bluetooth
			if (resultCode != Activity.RESULT_OK) {
				Log.d(TAG, "Do NOT enable bluetooth");
				finishDialogNoBluetooth();
			}
			break;
			
		case REQUEST_CONNECT_DEVICE :
			// when DeviceListAcitivity returns with a device to connect
			if (resultCode == Activity.RESULT_OK) {
				// get the MAC address and then connect to the device
				String address = data.getExtras()
						.getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				btService.connect(address);
			}
			break;
		}
	}
	
	/**
	 * Get user confirmation to enable bluetooth
	 */
	public void dialogEnableBluetooth() {
		
		Log.d(TAG, "getting confirmation to enable bluetooth");
		
    	if (!enablingBT) {
	    		
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    		builder.setMessage(R.string.alert_dialog_turn_on_bt)
    			.setIcon(android.R.drawable.ic_dialog_alert)
    			.setTitle(android.R.string.dialog_alert_title)
    			.setCancelable(false)
    			.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						enablingBT = true;
						Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
						startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
					}
				})
				.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finishDialogNoBluetooth();
					}
				});
    		AlertDialog alert = builder.create();
    		alert.show();	    	
    	}
	}
	
	private void sendData(String message) {
		
		Log.d(TAG, "Sending data: " + message + "...");
		byte[] msgBuffer = message.getBytes();
		if (msgBuffer.length > 0) {
			btService.write(msgBuffer);
		}
	}
	
	/**
	 * Displays an alert dialog to the user if Bluetooth is not available on the device
	 */
    public void finishDialogNoBluetooth() {
    	
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage(R.string.alert_dialog_no_bt)
    		.setIcon(android.R.drawable.ic_dialog_info)
    		.setTitle(R.string.app_name)
    		.setCancelable(false)
    		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {		
				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			});
    	AlertDialog alert = builder.create();
    	alert.show();
    }
    
    /**
     * Processes the messages returned from the Handler
     * @param msg <Message> the message object returned by the handler
     */
    void handleMessage(Message msg) {
    	
    	Context context = getApplicationContext();
    	
		switch (msg.what) {
		
		case BTSerialService.MESSAGE_NO_ADAPTER:
			finishDialogNoBluetooth();
			break;
			
		case BTSerialService.MESSAGE_ADAPTER_DISABLED:
			dialogEnableBluetooth();
			break;
		
		case BTSerialService.MESSAGE_STATE_CHANGE:
			
			Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
			switch(msg.arg1) {
			
			case BTSerialService.STATE_CONNECTED:
				setConnectMenuView(
						android.R.drawable.ic_menu_close_clear_cancel, R.string.action_disconnect);
				//sendData(context.getString(R.string.connected_message) + "\r\n");
				sendData("TIME\r\n");
				break;
				
			case BTSerialService.STATE_CONNECTING:
				// placeholder
				break;
				
			case BTSerialService.STATE_LISTEN:
			case BTSerialService.STATE_NONE:
				setConnectMenuView(
						android.R.drawable.ic_menu_search, R.string.action_connect);
				
				break;
			}
			break;
			
		case BTSerialService.MESSAGE_WRITE:
			try {
				String msgString = new String((byte[]) msg.obj, "UTF-8");
				Log.d(TAG, "Message sent: " + msgString);
			}
			catch(Exception e) {
				Log.d(TAG, "Could not convert sent bytes to String");
			}
			break;
			
		case BTSerialService.MESSAGE_READ:
			try {
				
				byte[] byteMsg = (byte[]) msg.obj;
				
				// find newline characters in byte string
				for (int idx = 0; idx < byteMsg.length; idx++) {
					if (byteMsg[idx] == 0x0A || byteMsg[idx] == 0x0D) {
						if (readBuffer.size() > 0) {
							Log.d(TAG, "Message received: " + readBuffer.toString());
							final TextView timeField = (TextView) findViewById(R.id.textTime);
							timeField.setText(readBuffer.toString());
							readBuffer.reset();
						}
					} else {
						readBuffer.write(byteMsg[idx]);
					}
				}
				
				// Find newline character in bytestring
				// If no newline, add entire string to read buffer
				// If newline, chop at newline (if newline not at end then add bit after newline to temp buffer
				// 		which will be added to the cleaned buffer after the completed buffer has been processed.
				// Add bit before newline to buffer, process buffer
				// Process buffer
				// Clear buffer
				// If any bytes after newline, add bytes to buffer
				
				// TODO Call message processor
			}
			catch(Exception e) {
				Log.d(TAG, "Could not convert received bytes to String: " + e.toString());
			}
			break;
			
		case BTSerialService.MESSAGE_DEVICE_NAME:
			// save the device name
			deviceName = msg.getData().getString(BTSerialService.KEY_DEVICE_NAME);
			Toast.makeText(context, getString(R.string.toast_connected_to) + " "
					+ deviceName, Toast.LENGTH_SHORT).show();
			break;
		}
	}
    
    /**
     * Handler for processing messages from BTSerialService
     */
    static class BTHandler extends Handler {
    	
    	private final WeakReference<MainActivity> mParent;
    	
    	BTHandler(MainActivity parent) {
    		mParent = new WeakReference<MainActivity>(parent);
    	}
    	
    	@Override
    	public void handleMessage(Message msg) {
    		
    		MainActivity activity = mParent.get();
    		
    		if (activity != null){
    			activity.handleMessage(msg);
    		}
    	}
    }
}
